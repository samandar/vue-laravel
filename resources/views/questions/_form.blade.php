@csrf
<div class="form-group">
    <label for="title">Question-Title</label>
    <input type="text" value="{{ old('title',$question->title) }}" name="title" id="question-title" class="form-control {{ $errors->has('title') ? 'is-invalid' : '' }}">

    @if($errors->has('title'))
        <div class="invalid-feedback">
            <span>{{ $errors->first('title') }}</span>
        </div>
    @endif
</div>
<div class="form-group">
    <label for="body">Question-Body</label>
    <textarea name="body" rows="6" id="question-body" class="form-control {{ $errors->has('body') ? 'is-invalid' : '' }}">{{ old('body',$question->body) }}
    </textarea>
    @if($errors->has('body'))
        <div class="invalid-feedback">
            <span>{{ $errors->first('body') }}</span>
        </div>
    @endif
</div>
<div class="form-group">
    <button type="submit" id="question-title" class="btn btn-outline-primary btn-lg">
        {{ $btnText }}
    </button>
</div>
