<?php
// HTTP
define('HTTP_SERVER', 'http://mona.rocketon.ru/admin/');
define('HTTP_CATALOG', 'http://mona.rocketon.ru/');

// HTTPS
define('HTTPS_SERVER', 'http://mona.rocketon.ru/admin/');
define('HTTPS_CATALOG', 'http://mona.rocketon.ru/');

// DIR
define('DIR_APPLICATION', '/var/www/rocketon/data/www/mona.rocketon.ru/upload/admin/');
define('DIR_SYSTEM', '/var/www/rocketon/data/www/mona.rocketon.ru/upload/system/');
define('DIR_IMAGE', '/var/www/rocketon/data/www/mona.rocketon.ru/upload/image/');
define('DIR_STORAGE', DIR_SYSTEM . 'storage/');
define('DIR_CATALOG', '/var/www/rocketon/data/www/mona.rocketon.ru/upload/catalog/');
define('DIR_LANGUAGE', DIR_APPLICATION . 'language/');
define('DIR_TEMPLATE', DIR_APPLICATION . 'view/template/');
define('DIR_CONFIG', DIR_SYSTEM . 'config/');
define('DIR_CACHE', DIR_STORAGE . 'cache/');
define('DIR_DOWNLOAD', DIR_STORAGE . 'download/');
define('DIR_LOGS', DIR_STORAGE . 'logs/');
define('DIR_MODIFICATION', DIR_STORAGE . 'modification/');
define('DIR_SESSION', DIR_STORAGE . 'session/');
define('DIR_UPLOAD', DIR_STORAGE . 'upload/');

// DB
define('DB_DRIVER', 'mysqli');
define('DB_HOSTNAME', 'localhost');
define('DB_USERNAME', 'furniture');
define('DB_PASSWORD', 'A7q6X9y2');
define('DB_DATABASE', 'furniture');
define('DB_PORT', '3306');
define('DB_PREFIX', 'oc_');

// OpenCart API
define('OPENCART_SERVER', 'https://www.opencart.com/');
