var wishlist = {
	'add': function(product_id) {
		$.ajax({
			url: 'index.php?route=account/wishlist/add',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {

				if (json['redirect']) {
					location = json['redirect'];
				}
				$.toast().reset('all');
				$.toast({
					heading: "Успешно",
					text: 'Успешно добавлено в список желаний',
					position: 'top-right',
					stack: false
				})
				setTimeout(function () {
					$('#wishlist-total #total-quantity').html(json['total']);
				}, 100);

				$('#wishlist-total > .basket-sidebar > .basket-box .basket-body').load('index.php?route=common/wish/info .basket-body');


			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
        });
	},
	'remove': function(product_id) {
		$.ajax({
			url: 'index.php?route=account/wishlist/remove',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
                
				$('.alert-dismissible').remove();

				if (json['redirect']) {
					location = json['redirect'];
				}
				$.toast().reset('all');
				$.toast({
					heading: "Успешно",
					text: 'Успешно удален из списка желаний',
					position: 'top-right',
					stack: false
				})
				setTimeout(function () {
					$('#wishlist-total #total-quantity').html(json['total']);
				}, 100);

				$('#wishlist-total > .basket-sidebar > .basket-box .basket-body').load('index.php?route=common/wish/info .basket-body');

                var wishlistId = ".wishlistById" + product_id;
                $(wishlistId).find('svg').toggleClass('active');
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
        });
	}
}

var wishlistModal = {
	'add': function(product_id) {

		$.ajax({
			url: 'index.php?route=account/wishlist/add',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
                
				if (json['redirect']) {
					location = json['redirect'];
				}
				$.toast().reset('all');
				$.toast({
					heading: "Успешно",
					text: 'Успешно добавлено в список желаний',
					position: 'top-right',
					stack: false
				})
				setTimeout(function () {
					$('#wishlist-total #total-quantity').html(json['total']);
				}, 100);

				$('#wishlist-total > .basket-sidebar > .basket-box .basket-body').load('index.php?route=common/wish/info .basket-body');


				var wishlistId = ".wishlistById" + product_id;
                $(wishlistId).find('svg').toggleClass('active');
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
        });

       
	},
	'remove': function(product_id) {
		$.ajax({
			url: 'index.php?route=account/wishlist/remove',
			type: 'post',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
                
				$('.alert-dismissible').remove();

				if (json['redirect']) {
					location = json['redirect'];
				}
				$.toast().reset('all');
				$.toast({
					heading: "Успешно",
					text: 'Успешно добавлено в список желаний',
					position: 'top-right',
					stack: false
				})
				setTimeout(function () {
					$('#wishlist-total #total-quantity').html(json['total']);
				}, 100);

				$('#wishlist-total > .basket-sidebar > .basket-box .basket-body').load('index.php?route=common/wish/info .basket-body');

                var wishlistId = ".wishlistById" + product_id;
                $(wishlistId).find('svg').toggleClass('active');
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
        });
	}
}


$('.--search_icon').on('click',function(){
    product_id = $(this).find('input').val();
    
    if(product_id){
        $.ajax({
			url: 'index.php?route=ajax/product',
			type: 'get',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
                data = json['success'];
                // for product and category name begin
                $('.modal--wrapper').find('.modal-body-left').find('.modal-body-title').find('p').html(data['category_name']);
                $('.modal--wrapper').find('.modal-body-left').find('.modal-body-title').find('h1').html(data['name']);
                // for product and category name end

                // for images begin
                $('.modal--wrapper').find('.modal-body-left').find('.modal-main-img').find('img').prop("src",data['image']);
                
                $resImage = '';
                for($i = 0; $i < data['images'].length; $i++) {
                    $resImage += `<div class="defaultImages"><img src="` + data['images'][$i] + `" alt="" class="d-block"></div>`;
                }
                $('.modal--wrapper').find('.modal-body-left').find('.modal-img-thumb').html($resImage);
                // for images end

				$('.modal--wrapper').find('.modal-body-right').find('.modal--cost').find('h1').html(data['tax']);

                // for size begin
                $('.modal--wrapper').find('.modal-body-right').find('.--product--size').find('.length').find('p').html(data['length']);
                $('.modal--wrapper').find('.modal-body-right').find('.--product--size').find('.width').find('p').html(data['width']);
                $('.modal--wrapper').find('.modal-body-right').find('.--product--size').find('.height').find('p').html(data['height']);
                // for size end

                // for description begin
                $('.modal--wrapper').find('.modal-body-right').find('.--product-desc').find('p').html(data['description']);
                // for description end
                
                // for href begin
                $('.modal--wrapper').find('.modal-body-right').find('.--product-view').find('a').prop('href',data['href']);
                // for href end

                $('.defaultImages').on('click', function(){
                    $('.modal--wrapper').find('.modal-body-left').find('.modal-main-img').find('img').prop("src",$(this).find('img').prop("src"));
                });

                // for wishlist begin
                $('.modal--wrapper').find('.modal-body-right').find('.--wishlist').unbind("click");
                
                $('.modal--wrapper').find('.modal-body-right').find('.--wishlist').on("click",function(){ wishlistModal.add(data['product_id']) });
				
				$('.--wishlist').click(function() {
					$(this).find('svg').toggleClass('active');
				});
				
                if(data['wishlist']){
					if(!$('.modal--wrapper').find('.modal-body-right').find('.--wishlist').find('svg').hasClass('active')){
                        $('.modal--wrapper').find('.modal-body-right').find('.--wishlist').find('svg').addClass('active');
                    }
                }
                else {
                    if($('.modal--wrapper').find('.modal-body-right').find('.--wishlist').find('svg').hasClass('active')){
                        $('.modal--wrapper').find('.modal-body-right').find('.--wishlist').find('svg').removeClass('active');
                    }        
                }
				// for wishlist end
				
				// form begin
				$('.modal--wrapper').find('.modal-body-right').find('form').find('.hidden-value').val(data['product_id']);
				// form end

			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		}); 
    }

});

$('.--fast-buy').on('click',function() {
	product_id = $(this).find('input').val();
	if(product_id){
        $.ajax({
			url: 'index.php?route=ajax/product',
			type: 'get',
			data: 'product_id=' + product_id,
			dataType: 'json',
			success: function(json) {
				data = json['success'];
				
				$('.fast-buy-modal').find('.fast-buy-form').find('p').html(data['name'] + ", " + data['tax']);
				$('.fast-buy-modal').find('.fast-buy-form').find('.hidden-value').val(data['product_id']);
				
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		}); 
    }
});

var cart = {
	'add': function(product_id, quantity) {
		$.ajax({
			url: 'index.php?route=checkout/cart/add',
			type: 'post',
			data: 'product_id=' + product_id + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
			dataType: 'json',
		
			success: function(json) {

				$('.alert-dismissible, .text-danger').remove();

				if (json['redirect']) {
					location = json['redirect'];
				}
				$.toast().reset('all');
				$.toast({
					heading: "Успешно",
					text: 'Успешно добавлено в корзину',
					position: 'top-right',
					stack: false
				})
				if (json['success']) {
					$('#content').parent().before('<div class="alert alert-success alert-dismissible"><i class="fa fa-check-circle"></i> ' + json['success'] + ' <button type="button" class="close" data-dismiss="alert">&times;</button></div>');

					// Need to set timeout otherwise it wont update the total
					setTimeout(function () {
						$('#cart').find('#cart-total').text(json['total']);
					}, 100);

					$('#cart > .basket-sidebar > .basket-box').load('index.php?route=common/cart/info .basket-box .basket--title,.basket-box .basket-body,.basket-box .basket-footer');
				}

			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});


	},
	'update': function(key, quantity) {
		$.ajax({
			url: 'index.php?route=checkout/cart/edit',
			type: 'post',
			data: 'key=' + key + '&quantity=' + (typeof(quantity) != 'undefined' ? quantity : 1),
			dataType: 'json',
			success: function(json) {

			setTimeout(function () {
				$('#cart').find('#cart-total').text(json['total']);
			}, 100);

			if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
				location = 'index.php?route=checkout/cart';
			} else {
				$('#cart > .basket-sidebar > .basket-box').load('index.php?route=common/cart/info .basket-box .basket--title,.basket-box .basket-body,.basket-box .basket-footer');
			}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	},
	'remove': function(key) {
		$.ajax({
			url: 'index.php?route=checkout/cart/remove',
			type: 'post',
			data: 'key=' + key,
			dataType: 'json',

			success: function(json) {
				// Need to set timeout otherwise it wont update the total
				$.toast().reset('all');
				$.toast({
					heading: "Успешно",
					text: 'Успешно удален из корзины',
					position: 'top-right',
					stack: false
				})
				setTimeout(function () {
					$('#cart').find('#cart-total').text(json['total']);	
				}, 100);

				if (getURLVar('route') == 'checkout/cart' || getURLVar('route') == 'checkout/checkout') {
					location = 'index.php?route=checkout/cart';
				} else {
					$('#cart > .basket-sidebar > .basket-box').load('index.php?route=common/cart/info .basket-box .basket--title,.basket-box .basket-body,.basket-box .basket-footer');
				}
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
	},


	
}


function getURLVar(key) {
	var value = [];

	var query = String(document.location).split('?');

	if (query[1]) {
		var part = query[1].split('&');

		for (i = 0; i < part.length; i++) {
			var data = part[i].split('=');

			if (data[0] && data[1]) {
				value[data[0]] = data[1];
			}
		}

		if (value[key]) {
			return value[key];
		} else {
			return '';
		}
	}
}


$("#callBackForm").submit(function(e) {
	e.preventDefault();
	
	var form = $(this);
	var url = $(this).attr('action');

	$.ajax({
		type: "POST",
		url:  url,
		data: form.serialize(),
		success: function(data) {
			$.toast().reset('all');
            $.toast({
				heading: "Успешно",
				text: data,
				position: 'top-center',
                stack: false
			})
			$("#callBackForm").trigger("reset");
			$(".call-wrapper, .--user-dropdown").removeClass('d-block');
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$("#questionForm").submit(function(e) {
	e.preventDefault();
	
	var form = $(this);
	var url = $(this).attr('action');

	$.ajax({
		type: "POST",
		url:  url,
		data: form.serialize(),
		success: function(data) {
			$.toast().reset('all');
            $.toast({
				heading: "Успешно",
				text: data,
				position: 'top-center',
                stack: false
			})
			$("#questionForm").trigger("reset");
			$(".call-wrapper, .--user-dropdown").removeClass('d-block');
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

$("#forProductForm").submit(function(e) {
	e.preventDefault();
	
	var form = $(this);
	var url = $(this).attr('action');

	$.ajax({
		type: "POST",
		url:  url,
		data: form.serialize(),
		success: function(data) {

			$('.basket-sidebar, body').removeClass('active');

			$('.basket-sidebar-overlay, .fast-buy-modal').fadeOut(500);

			$.toast().reset('all');
            $.toast({
				heading: "Успешно",
				text: data,
				position: 'top-center',
                stack: false
			})
			$("#forProductForm").trigger("reset");
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});
$("#forQuickViewProductForm").submit(function(e) {
	e.preventDefault();
	
	var form = $(this);
	var url = $(this).attr('action');

	$.ajax({
		type: "POST",
		url:  url,
		data: form.serialize(),
		success: function(data) {

			$('.basket-sidebar, body').removeClass('active');
			$('.--form-quick-view').slideUp();
			$('body').removeClass('active');
			$('.modal--wrapper').removeClass('d-block');

			$.toast().reset('all');
            $.toast({
				heading: "Успешно",
				text: data,
				position: 'top-center',
                stack: false
			})
			$("#forQuickViewProductForm").trigger("reset");
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});


$('#clearWishlist').on('click' ,function() {
	$.ajax({
		url: 'index.php?route=account/wishlist/removeAll',
		type: 'get',
		dataType: 'json',
		success: function(json) {
			
			$('.alert-dismissible').remove();

			if (json['redirect']) {
				location = json['redirect'];
			}

			setTimeout(function () {
				$('#wishlist-total #total-quantity').html(json['total']);
			}, 100);

			$('#wishlist-total > .basket-sidebar > .basket-box .basket-body').load('index.php?route=common/wish/info .basket-body');

			for($i = 0; $i < json['wishlists'].length; $i++){
				var wishlistId = ".wishlistById" + json['wishlists'][$i];
				$(wishlistId).find('svg').toggleClass('active');
			}

		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
});

function searchValue(value) {

	var form = $('#searchForm');
	var url = form.attr('action');

	$.ajax({
		type: "GET",
		url:  url,
		data: form.serialize(),
		success: function(data) {
			products = data['products'];
			$result = '';
			for($i = 0; $i < products.length; $i++ ) {
				$result += `<li>
								<a href="` + products[$i]['href'] + `">
									<div>
										<img src="` +products[$i]['thumb']+ `" alt="" width="90" height="60">
									</div>
									<div class="flex-1 ul-product-desc">
										<p>` + products[$i]['name'] + `</p>
										<span class="d-block">`
											+ products[$i]['tax'] +
										`</span>
									</div>
								</a>
							</li>`;
			}
			if($result == '') {
				$result += `<p class="text-center" style="margin: 20px auto;">Ничего не найдено !!!</p>`;
			}
			$('.--search-wrapper-ul-product').html($result);
			// console.log(data);
		},
		error: function(xhr, ajaxOptions, thrownError) {
			alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});	
}

$('#search-button').on('click',function() {
	var url = $('base').attr('href') + 'index.php?route=product/search';
	var value = $('#fixed-search').val();

	if (value) {
		url += '&search=' + encodeURIComponent(value);
	}

	location = url;
});

$('#fixed-search').keypress(function(event) {
	if (event.keyCode == 13) {
		event.preventDefault();
	}
});




