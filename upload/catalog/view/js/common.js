
$(document).ready(function() {

    // let i = -38;
    // $( ".--logo-hover" ).hover(
    //     function() {
    //         $('.--logo-img').css({
    //             'background-position-y' : i
    //         });
    //         i = i - 38;
    //     }, function() {
    //         console.log('Moved out');
    //     }
    // );

    $( "#fixed-search" ).keyup(function() {
        if( $(this).val().length > 0 ) {
            $('.--search-wrapper').css({
                'visibility': 'visible'
            });
            searchValue($(this).val());
        }
        if( $('.--search-wrapper-ul-category').height() + $('.--search-wrapper-ul-product').height() >= $('.--search-wrapper-body').height() ) {
            $('.--search-wrapper-body').css({
                'overflow-y' : 'scroll'
            });
        }
    });

    $('.--search-wrapper-header > svg').click(function(){
        $('.--search-wrapper').css({
            'visibility': 'hidden'
        });
    });

    $( "#fixed-search" ).click(function() {
        if( $(this).val().length > 0 ) {
            $('.--search-wrapper').css({
                'visibility': 'visible'
            });
        }
    });

    $(document).click(function(event) {
        if (!$(event.target).closest("#fixed-search, .--search-wrapper").length) {
            $('.--search-wrapper').css({
                'visibility': 'hidden'
            });
        }
    });

    $('.--navigation-menu').click(function() {
        $('.--default-menu .fixed-bar').toggleClass('active');
    });

    $('.--default-menu .--search-items').click(function() {
        if( !$('.--default-menu .fixed-bar').is('active') ) {
            $('.--default-menu .fixed-bar').addClass('active');
        }
    });

    // Cache selectors
    var $accordion = $(".js-accordion");
    var $allPanels = $(" .accordion-panel").hide();
    var $allItems = $(".accordion-item");

    // Event listeners
    $accordion.on("click", ".accordion-toggle", function () {
    // Toggle the current accordion panel and close others
        $allPanels.slideUp();
        $allItems.removeClass("is-open");
        if ($(this).next().is(":visible")) {
            $(".accordion-panel").slideUp();
        }else {
            $(this).next().slideDown().closest(".accordion-item").addClass("is-open");
        }
        return false;
    });

    $(window).scroll(function() {
        if( $(this).scrollTop() > 60 ) {
           $('.top-header-wrapper').addClass('active');
        }
        else {
            $('.top-header-wrapper').removeClass('active');
        }

        if( $(this).scrollTop() > 125 ) {
            $('.product--basket_info > form').addClass('active');

            if($('.product--basket > div').height() < $('.product--basket_info > form').height() + $(this).scrollTop() ) {
                $('.product--basket_info ').addClass('relative');
                $('.product--basket_info > form.active').addClass('bottom-side');
            }
            else {
                $('.product--basket_info ').removeClass('relative');
                $('.product--basket_info > form.active').removeClass('bottom-side');
            }
        }
        else {
            $('.product--basket_info > form').removeClass('active');
        }
    });

    $('.--menu > ul > li:first-child').click(function() {
        $(this).addClass('active');
    });

    $(document).click(function(event) {
        if (!$(event.target).closest(".--menu > ul > li:first-child, .--menu > ul > li:first-child").length) {
          $(".--menu > ul > li:first-child").removeClass("active");
        }
    });

    $('.--menu ul li').click(function() {
        $('.--menu ul li').removeClass('active');
        $('.--menu ul li .--menu--items-slide').slideUp();
        $(this).addClass('active');
        $(this).children('.--menu--items-slide').slideDown();
    });

    $(document).click(function(event) {
        if (!$(event.target).closest(".--menu ul li .--menu--items-slide, .--menu ul li").length) {
          $(".--menu ul li .--menu--items-slide").slideUp();
        }
    });

    $('.top-header-list ul > li:first-child a').click(function(e) {
        e.preventDefault();
        $('.call-wrapper').toggleClass('d-block');
    });

    $('.--user, .catalog-sort-money').click(function() {
        $('.--user-dropdown, .--sort-dropdown').toggleClass('d-block');
    });

    $(document).click(function(event) {
        if (!$(event.target).closest(".catalog-sort-money, .--sort-dropdown").length) {
          $(".--sort-dropdown").removeClass('d-block');
        }
    });

    $(document).click(function(event) {
        if (!$(event.target).closest(".top-header-list ul > li:first-child a, .--user , .call-wrapper, .--user-dropdown").length) {
          $(".call-wrapper, .--user-dropdown").removeClass('d-block');
        }
    });

    $('.__same-modal').click(function() {
        $('body').addClass('active');
        $(this).children('.basket-sidebar-overlay').fadeIn(1000);
        $(this).children('.basket-sidebar').addClass('active');
    });

    $('.basket-close, .fast-buy-modal-close').click(function(e) {
        e.stopImmediatePropagation();
        $('.basket-sidebar, body').removeClass('active');
        setTimeout(function() {
            $('.basket-sidebar-overlay, .fast-buy-modal').fadeOut(1000);
        }, 500);
    });

    $('.--fast-buy, .checkout-order button').click(function(e) {
        e.preventDefault();
        $('body').addClass('active');
        $('.fast-buy-modal').prev().fadeIn(1000);
        setTimeout(function() {
            $('.fast-buy-modal').fadeIn(500);
        }, 500);
    });

    // Quick View Modal
    $('.--search_icon').click(function() {
        $('.modal--wrapper').addClass('d-block');
        setTimeout(function() {
            $('body').addClass('active');
        }, 600);
    });

    $('.modal--close').click(function() {
        $('body').removeClass('active');
        $('.modal--wrapper').removeClass('d-block');
    });
    // End Quick View Modal

    $('.--heart_icon svg').click(function() {
        $(this).toggleClass('active');
    });

    $('.--wishlist').click(function() {
        $(this).find('svg').toggleClass('active');
    });

    $('.-cost--btn').click(function() {
        $('.--form-quick-view').slideDown();
    });

    $('.-cost-down').click(function() {
        $('.--form-quick-view').slideUp();
    });

    let _mainImg = $('.--hero-img img');

    $('.--thumb-imgs > div').click(function() {
        $('.--thumb-imgs > div').removeClass('active');
        $('.--hero-img').removeClass('active');
        $(this).addClass('active');
        let _src = $(this).find('img').attr('src');
        setTimeout(function() {
            _mainImg.attr('src', _src);
            $('.--hero-img').addClass('active');
        },200);
    });

    $('.--tabs-ul li').click(function() {
        $('.--tabs-ul li').removeClass('active');
        $(this).addClass('active');
    });

    $('.wrapper--product').css({
        'min-height' : $('.buy--info').height() - 150
    });

    $('.catalog--filter').click(function() {
        $('.catalog--filters').toggleClass('active');
    });

    $('.--filter-color').click(function() {
        $(this).children().children().toggleClass('active');
        if($(this).find("input[type=checkbox]").is(':checked'))
            $(this).find("input[type=checkbox]").prop("checked",false);
        else
            $(this).find("input[type=checkbox]").prop("checked",true);
    });

    $('.catalog--filter').click(function() {
        $('.--catalog-container-items').toggleClass('active');
    });

    $('.hero--slider .owl-carousel').owlCarousel({
        loop:true,
        nav:true,
        dots: false,
        items:1
    });

    $('.top-sale-slider .owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:true,
        dots: false,
        responsive:{
            0: {
                items:2
            },
            480: {
                items:2
            },
            768:{
                items:3
            },
            1400:{
                items:4
            },
            1920:{
                items:5
            }
        }
    });

    $('.top-sale-slider .owl-carousel .owl-nav').removeClass('disabled');

    $('.--ins-gallery .owl-carousel').owlCarousel({
        loop:true,
        margin:10,
        nav:false,
        responsive:{
            0:{
                items:1
            },
            768:{
                items:3
            },
            1000:{
                items:3
            },
            1920: {
                items: 5
            }
        }
    })

    $('.grid-quarter').imagefill();

    $('.--phone').mask('+000 (00) 000 00 00', {placeholder: "+998 (XX) XXX XX XX"});

    $('.wrapper-items .wrapper-item').last().hide();
    $('.call-wrapper > ul > li').click(function() {
        let _index = $(this).index();
        $('.call-wrapper > ul > li').removeClass('active');
        $(this).addClass('active');
        $('.wrapper-items .wrapper-item').eq(_index).siblings().hide();
        $('.wrapper-items .wrapper-item').eq(_index).show();
    });

    function incrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);
        var cart_id = parseInt(parent.find('input[name=quantityCartId]').val(), 10)

        if (!isNaN(currentVal)) {
            parent.find('input[name=' + fieldName + ']').val(currentVal + 1);
        } else {
            parent.find('input[name=' + fieldName + ']').val(0);
        }
        cart.update(cart_id,currentVal + 1);
    }

    function decrementValue(e) {
        e.preventDefault();
        var fieldName = $(e.target).data('field');
        var parent = $(e.target).closest('div');
        var currentVal = parseInt(parent.find('input[name=' + fieldName + ']').val(), 10);
        var cart_id = parseInt(parent.find('input[name=quantityCartId]').val(), 10)

        if (!isNaN(currentVal) && currentVal > 0) {
            parent.find('input[name=' + fieldName + ']').val(currentVal - 1);
        } else {
            parent.find('input[name=' + fieldName + ']').val(0);
        }
        cart.update(cart_id,currentVal - 1);
    }

    $(document).on('click','.button-plus',function(e) {
        incrementValue(e);
    });
    $(document).on('click','.button-minus',function(e) {
        decrementValue(e);
    });

    $('.--type-shipping > label > input, .--shipping-items > label > input').click(function() {
        $('.--type-shipping > label > input, .--shipping-items > label > input').siblings().removeClass('active');
        $(this).siblings().addClass('active');
    });

    $('.shiping--2').click(function() {
        $(this).parent().next().slideUp();
    });
    $('.shiping--1').click(function() {
        $(this).parent().next().slideDown();
    });

    $('.--change-conf').click(function() {
      $('body').addClass('active');
      setTimeout(function() {
          $('.--change--conf').addClass('active');
      },600)
      $('.--overlay').fadeIn(700);
    });

    $('.close--sidebar').click(function() {
      $('body').removeClass('active');
      $('.--change--conf').removeClass('active');
      $('.--overlay').fadeOut(700);
    });

    // Conf Tabs
    $('.--conf-tabs-content').hide();
    $('.--conf-tabs-content').first().show();

    $('.--conf-tabs-li li').click(function() {
      let _currentLi = $(this).index();
      $('.--conf-tabs-li li').removeClass('active');
      $(this).addClass('active');

      $('.--conf-tabs-content').hide();
      $('.--conf-tabs-content').eq(_currentLi).show();
    });
    // End Conf Tabs

    $('.--info-toggle').click(function() {
        // $('.--conf-info').slideUp();
        $(this).closest('div').next().slideToggle();
    });

    $('.color-items_item').click(function() {
        $('.color-items_item').removeClass('active');
        $('.--main-img-box').removeClass('active');
        $(this).addClass('active');
        let _currentImg = $(this).data().img;
        let _currentBg = $(this).data().bg;

        $('<img/>', {
            class: '--collection-list-items fadeIn',
            src: _currentBg,
            'data-img': _currentImg
        }).appendTo('.--collection-li');

        setTimeout(function() {
            $('.--main-img-box').addClass('active');
            $('.--main-img-box img').attr('src', _currentImg);
        }, 500);
    });

    $( "body" ).on( "click", ".--collection-list-items", function() {
        $('.--main-img-box img').attr('src', $(this).data().img);
    });

    $(".--change--conf").on( 'scroll', function(e){
        if( $(this).scrollTop() > 60 ) {
            $('.--change-conf-header').addClass('active');
        }
        else {
            $('.--change-conf-header').removeClass('active');
        }
    });
    

    $('.--tabs_content').hide();
    $('.--tabs_content-service').hide();

    $('.--tabs_list li').click(function() {
        let _this = $(this).index();
        $('.--tabs_list li').removeClass('active');
        $(this).addClass('active');

        $('.--tabs_content').hide();
        $('.--tabs_content').eq(_this).show();
    });

    $('.--tabs_content').first().show();
    $('.--tabs_content-service').first().show();

    $('.--service_list li').click(function() {
        let _this = $(this).index();
        $('.--service_list li').removeClass('active');
        $(this).addClass('active');

        $('.--tabs_content-service').hide();
        $('.--tabs_content-service').eq(_this).show();
    });

    $( ".--catalog-container-item" ).hover(
        function() {
            $( this ).find('.--catalog-container-item-hover').fadeIn(500);
        }, function() {
            $( this ).find('.--catalog-container-item-hover').fadeOut(0);
        }
    );
    
    $('.--default-menu .--menu > ul > li:not(:first-child)').click(function() {
        $('.--default-menu .fixed-bar').addClass('active');
    });
});
