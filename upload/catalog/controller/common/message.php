<?php
class ControllerCommonMessage extends Controller {
	private $error = array();
    
    public function add() {
        $this->load->language('common/message');
        $this->load->model('common/message');

        $this->model_common_message->addMessage($this->request->post);

        $json = $this->language->get('text_success');
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getQuantity() {
        $this->load->model('common/message
        ');
		$results = $this->model_common_message->getNotViewed()['COUNT(*)'];

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($results));
	}

    // public function validateForm(){

    // }

}
