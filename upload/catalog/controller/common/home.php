<?php
class ControllerCommonHome extends Controller {
	public function index() {
		
		$this->document->setTitle($this->config->get('config_meta_title'));
		$this->document->setDescription($this->config->get('config_meta_description'));
		$this->document->setKeywords($this->config->get('config_meta_keyword'));

		if (isset($this->request->get['route'])) {
			$this->document->addLink($this->config->get('config_url'), 'canonical');
		}

		
		$this->load->model('design/banner');
		$this->load->model('tool/image');

		$this->load->model('catalog/product');
		$this->load->model('catalog/category');

		$data['banners'] = array();
		$data['banner_2s'] = array();
		$data['products'] = array();

		$results = $this->model_design_banner->getBanner(7);

		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$data['banners'][] = array(
					'title' => $result['title'],
					'link'  => $result['link'],
					'image' => 'image/'.$result['image']
				);
			}
		}

		$result_banner_2s = $this->model_design_banner->getBanner(9);

		foreach ($result_banner_2s as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$data['banner_2s'][] = array(
					'title' => $result['title'],
					'link'  => $result['link'],
					'image' => 'image/'.$result['image']
				);
			}
		}

		$result_products = $this->model_catalog_product->getHitProducts(6);

		if ($result_products) {
			foreach ($result_products as $result) {
				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], 120, 120);
				} else {
					$image = $this->model_tool_image->resize('placeholder.png');
				}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = $result['rating'];
				} else {
					$rating = false;
				}
				
				$category = $this->model_catalog_product->getCategories($result['product_id']);
			
				if($category){
					$category_item = $this->model_catalog_category->getCategory($category[0]['category_id']);	
				}
				if($special){
					$percent_special = number_format(100 - ($special/$price)*100,1,'.',' ');
				}

				
				$data['products'][] = array(
					'product_id'  => $result['product_id'],
					'thumb'       => $image,
					'name'        => $result['name'],
					'description' => utf8_substr(trim(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8'))), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..',
					'price'       => $price,
					'special'     => $special,
					'tax'         => $tax,
					'rating'      => $rating,
					'percent_special' => $percent_special,
					'length'	  => (float)$result['length'],
					'width'	  	  => (float)$result['width'],
					'height'	  => (float)$result['height'],
					'category_name' => $category_item['name'],
					'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id'])
				);
			}
		}
		
		// echo json_encode($data['products']);
		// exit();

		$advertises = $this->model_design_banner->getBanner(10);

		foreach ($advertises as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$data['advertises'][] = array(
					'title' => html_entity_decode($result['title']),
					'link'  => $result['link'],
					'image' => 'image/'.$result['image']
				);
			}
		}
		$data['wishlistProducts'] = $this->session->data['wishlist'];
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');

		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer_default');
		$data['header_home'] = $this->load->controller('common/header_home');

		$this->response->setOutput($this->load->view('common/home', $data));
	}
}
