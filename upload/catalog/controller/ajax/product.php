<?php
class ControllerAjaxProduct extends Controller {
	private $error = array();

    public function index(){
        $this->load->model('tool/image');
        if (isset($this->request->get['product_id'])) {
			$product_id = (int)$this->request->get['product_id'];
		} else {
			$product_id = 0;
        }
        
        $this->load->model('catalog/product');
        $this->load->model('catalog/category');
        $this->load->model('tool/image');
        $product_info = $this->model_catalog_product->getProduct($product_id);
        $images = $this->model_catalog_product->getProductImages($product_id);


        $product_info['product_id'] = $product_id;
        $product_info['description'] = utf8_substr(trim(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8'))), 0, $this->config->get('theme_' . $this->config->get('config_theme') . '_product_description_length')) . '..';
        $product_info['length']	     = (float)$product_info['length'];
        $product_info['width']	     = (float)$product_info['width'];
        $product_info['height']	     = (float)$product_info['height'];
        $product_info['href']        = html_entity_decode($this->url->link('product/product', 'product_id='.$product_info['product_id']));
        $category = $this->model_catalog_product->getCategories($product_id);

        if($category){
            $category_item = $this->model_catalog_category->getCategory($category[0]['category_id']);	
        }


        if ((float)$product_info['special']) {
            $product_info['special'] = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
        } else {
            $product_info['special'] = false;
        }

        if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
            $product_info['price'] = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
        } else {
            $product_info['price'] = false;
        }

        if ($this->config->get('config_tax')) {
            $product_info['tax'] = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
        } else {
            $product_info['tax'] = false;
        }

        $product_info['images'] = [];
        array_push($product_info['images'],$this->model_tool_image->resize($product_info['image'], 80, 50));
        foreach($images as $image){
            array_push($product_info['images'],$this->model_tool_image->resize($image['image'], 80, 50));
        }

        if ($product_info['image']) {
            $product_info['image'] = $this->model_tool_image->resize($product_info['image'], 350, 200);
        } else {
            $product_info['image'] = $this->model_tool_image->resize('placeholder.png');
        }

        if(in_array($product_info['product_id'],$this->session->data['wishlist'])){
            $product_info['wishlist'] = true;
        }
        else{
            $product_info['wishlist'] = false;
        }
        
        $json['success']= $product_info;
        $json['success']['category_name'] = $category_item['name'];
        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }


}
