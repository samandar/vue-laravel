<?php

namespace App\Http\Controllers;
use App\Question;
use App\Answer;
use Illuminate\Http\Request;

class AnswersController extends Controller
{

    public function __construct() {
        $this->middleware('auth',['except' => ['index','show']]);
    }

    public function index(Question $question)
    {
        return $question->answers()->with('user')->paginate(2);
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Question $question,Request $request)
    {

        $answer = $question->answers()->create(
            $request->validate([
                'body' => 'required'
            ]) + ['user_id' => \Auth::id()]);

        if($request->expectsJson()){
            return response()->json([
                'message' => 'Successfully saved answer',
                'answer'  => $answer->load('user')
            ]);
        }

        return back()->with('success',"Your answer saved");

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question,Answer $answer)
    {
        $this->authorize('update',$answer);

        return view('answers.edit',compact('question','answer'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Question $question, Answer $answer)
    {
        $this->authorize('update',$answer);

        $answer->update($request->validate([
            'body' => 'required',
        ]));

        if($request->expectsJson()){
            return response()->json([
                'message' => 'Updated',
                'body_html' => $answer->body_html
            ]);
        }

        return redirect()->route('questions.show',$question->slug)->with('success','Successfully updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Answer  $answer
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question,Answer $answer)
    {
        $this->authorize('delete',$answer);

        $answer->delete();

        if(request()->expectsJson()){
            return response()->json([
                'message' => 'Removed',
            ]);
        }
        return back()->with(['success' => 'Successfully deleted']);
    }
}
