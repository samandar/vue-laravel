<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Answer;
class AcceptAnswerController extends Controller
{
    public function __invoke(Answer $answer) {
        $answer->question->acceptBestAnswer($answer);

        if(request()->expectsJson()) {
            return response()->json([
                'message' => 'You have accepted this answer best answer'
            ]);
        }

        return back();
    }
}
